import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShipperCreateComponent } from './components/shipper-create/shipper-create.component';
import { ShipperEditComponent } from './components/shipper-edit/shipper-edit.component';
import { ShipperListComponent } from './components/shipper-list/shipper-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ShipperCreateComponent,
    ShipperEditComponent,
    ShipperListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
