import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShipperCreateComponent } from './components/shipper-create/shipper-create.component';
import { ShipperEditComponent } from './components/shipper-edit/shipper-edit.component';
import { ShipperListComponent } from './components/shipper-list/shipper-list.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'shipper-list' },
  { path: 'create-shipper', component: ShipperCreateComponent },
  { path: 'shipper-list', component: ShipperListComponent },
  { path: 'shipper-edit/:id', component: ShipperEditComponent } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
